#Trabalho de APS

Constituição do grupo de trabalho:

* Diogo Esteves Nº12962
* Marco Fernandes Nº12156
* Manuel Paiva Nº12161

Tema do projecto: Sistema De Gestão de Faltas (Docentes e Alunos)

Entregas do Trabalho:

1. [Entrega 01](https://bitbucket.org/IM_a12161_Fernando_Paiva/repo_im_12156_12161_12962/wiki/Entrega%2001%20-%20Vis%C3%A3o%20e%20Planeamento%20Inicial%20do%20Projecto)

2. [Entrega 02](https://bitbucket.org/IM_a12161_Fernando_Paiva/repo_im_12156_12161_12962/wiki/Entrega%2002%20-%20An%C3%A1lise%20e%20planeamento)

3. [Entrega 03](https://bitbucket.org/IM_a12161_Fernando_Paiva/repo_im_12156_12161_12962/wiki/Entrega%2003%20-%20Implementa%C3%A7%C3%A3o%20do%20Projecto)
# **Entrega 03 - Implementação do Projecto** #

# **Análise, implementação e teste de cada Caso de Uso ** #

## ** Diagrama de Classes ** ##
![Entregas03.jpg](https://bitbucket.org/repo/9xxgG7/images/1710411466-Entregas03.jpg)

## **UserStories** ##

* **Registo de Presenças**

**Quem?** Professor

**Quando?** Registar aula

**Descrição:** O professor começa por se identificar no dispositivo carregando no botão verde e passando o cartão. Depois disto, os alunos passam os seus cartões e ficam registados na aula.
Por fim, o docente fecha a aula e escreve o sumário.

* **BackOffice** 

**Quem?** Funcionário

**Quando?** Trocar aula

**Descrição:** Inicialmente o funcionário identifica-se com o seu número e recebe os pedidos de troca de aula. Depois, realiza a troca de aula indicando as UC´s e as datas e horas propostas para a respetiva troca. Por fim, valida a troca.

## ** MockUps: ** ##

* ### **Registo de Presenças** ###

![Login.png](https://bitbucket.org/repo/9xxgG7/images/3788802387-Login.png)

Nesta janela existem duas escolhas após a identificação que dão origem a caminhos diferentes, um para o docente e outro para o aluno

![Registar Aula.png](https://bitbucket.org/repo/9xxgG7/images/3722073009-Registar%20Aula.png)

Nesta opção o docente tem de selecionar "Registar Aula" para efectuar o registo da mesma

![Registar Presença.png](https://bitbucket.org/repo/9xxgG7/images/3857541192-Registar%20Presen%C3%A7a.png)

Nesta janela o docente efetua o registo de aula, tendo ainda as possibilidades de fechar a aula e após esse fecho escrever o sumário

![Registar Presença - Aluno.png](https://bitbucket.org/repo/9xxgG7/images/2805661132-Registar%20Presen%C3%A7a%20-%20Aluno.png)

Esta janela é relativa ao aluno, e descreve o registo de presença na aula do mesmo

![Escrever Sumário.png](https://bitbucket.org/repo/9xxgG7/images/1817933158-Escrever%20Sum%C3%A1rio.png)

Esta janela corresponde ao registo do sumário por parte do docente após efetuar o fecho da aula, tem ainda acesso há lista de presenças dos alunos na respectiva aula

![Adicionar Presenças.png](https://bitbucket.org/repo/9xxgG7/images/3670652718-Adicionar%20Presen%C3%A7as.png)

O docente pode ainda adicionar presenças manualmente caso o aluno se tenha esquecido do cartão de identificação

![Confirmação.png](https://bitbucket.org/repo/9xxgG7/images/3392910497-Confirma%C3%A7%C3%A3o.png)

Esta ultima janela serve apenas como confirmação dos dados inseridos

* ### ** BackOffice ** 

![Plataforma Siga.png](https://bitbucket.org/repo/9xxgG7/images/4242404822-Plataforma%20Siga.png)

Esta janela corresponde ao login na plataforma SIGA, podendo escolher o caminho a seguir, podendo ser o caminho da opção docente ou funcionário dos serviços

![Docente.png](https://bitbucket.org/repo/9xxgG7/images/1911145878-Docente.png)

Nesta janela são demonstradas as possbilidades que o docente possui

![Funcionário.png](https://bitbucket.org/repo/9xxgG7/images/937745234-Funcion%C3%A1rio.png)

Nesta janela, é demonstrado o ambiente do docente aquando o login na plataforma

![Gerar Mapas.png](https://bitbucket.org/repo/9xxgG7/images/2141543553-Gerar%20Mapas.png)

Esta opção pertence ao docente na qual é possível o mesmo gerar os mapas das aulas com as presenças e o sumário

![Pedidos Troca de Aula.png](https://bitbucket.org/repo/9xxgG7/images/2786730125-Pedidos%20Troca%20de%20Aula.png)

Nesta opção, o funcionário tem a possibilidade de verficar os pedidos de trocas de aula existentes 

![Confirmação.png](https://bitbucket.org/repo/9xxgG7/images/2212931489-Confirma%C3%A7%C3%A3o.png)

Esta janela é uma janela de confirmação de dados

![Troca de Aula.png](https://bitbucket.org/repo/9xxgG7/images/3116320325-Troca%20de%20Aula.png)

Com base na lista de pedidos de troca de aula o funcionário através desta janela efectua a troca da mesma especificando todos os atribtutos

![Presenças.png](https://bitbucket.org/repo/9xxgG7/images/3885554598-Presen%C3%A7as.png)

O docente pode pedir a consulta de mapas nesta janela podendo escolher a aula respetiva e apresentar ou sumarios ou presenças

![Mapas.png](https://bitbucket.org/repo/9xxgG7/images/4264773692-Mapas.png)

Nesta janela aparecem as presenças solicitadas

![Mapas2.png](https://bitbucket.org/repo/9xxgG7/images/3759630937-Mapas2.png)

Nesta janela aparece o sumário da aula respetiva

![Obter Planificação.png](https://bitbucket.org/repo/9xxgG7/images/2959204957-Obter%20Planifica%C3%A7%C3%A3o.png)

Nesta janela é possível escolher as preferências para a planificação que o docente deseja obter

![Planificação.png](https://bitbucket.org/repo/9xxgG7/images/3548338097-Planifica%C3%A7%C3%A3o.png)

Nesta janela temos então a planificação pedida
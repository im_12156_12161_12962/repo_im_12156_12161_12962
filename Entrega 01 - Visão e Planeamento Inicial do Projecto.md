# Objectivos de Projecto
* ###Objectivo do Sistema de Software:
O sistema de Gestão de Faltas/Presença relativa aos docentes e alunos tem como objectivo melhorar a gestão e registo destes dados. 
Os benefícios ou resultados pretendidos são relativos a melhorar primeiramente a forma como os dados são registos e posteriormente possibilitar um maior facilidade na pesquisa de uma informação nestes registos, 
de modo a que a mesma seja bem sucedida e eficaz.

* ###Identificação de partes interessadas:
Como parte interessada neste sistema temos a entidade que trata deste processo moroso (funcionários da direcção a escola respectiva), os docente pois será mais fácil controlar as presenças e os alunos.

* ###Diagrama de Contexto do Sistema
![DiagramaContexto.jpg](https://bitbucket.org/repo/9xxgG7/images/342688366-DiagramaContexto.jpg)

##Modelação dos processos de negócios
* ###Descrição dos processos de negócio suportadas

Os processos incluídos no sistema escolhido pelo grupo consistem primeiramente no registo de presenças dos alunos às aulas bem como dos docentes. O processo é descrito pela abertura da aula no dispositivo presente na sala. Para efectuar esta abertura da aula o docente dispõe de 10 minutos antes da hora de inicio da aula e 20 minutos depois. Segue-se depois a passagem dos cartões identificadores de cada aluno no mesmo, para assim registarem a sua presença, sendo que estes dispõe de 30 minutos depois do docente abrir a aula. Este processo repete-se para as varias unidades curriculares. No fim da aula, o professor através do dispositivo fecha o registo de presenças, e os dados dos alunos presentes na aula são enviados para a plataforma. Através dessa plataforma, o docente insere o sumário com o temas leccionados na respectiva aula, sendo que para esta acção tem um prazo de 3 dias após a abertura da aula para efectuar o registo desta informação.

* ###Diagrama de actividade

![diagrama_atividades.jpg](https://bitbucket.org/repo/9xxgG7/images/1011570939-diagrama_atividades.jpg)

##Modelo de Domínio
* ###Diagrama de Classes UML inicial para capturar entidades de negócio

![Diagrama de dominio.jpg](https://bitbucket.org/repo/9xxgG7/images/3693793992-Diagrama%20de%20dominio.jpg)

* ###Definição de conceitos, termos e entidades

**Conceitos**:

1. abrir e fechar aula, registar presenças e sumários

**Termos:**

1. Cartão: aluno tem um cartão assim como o docente, nesse cartão estão registados o numero e nome do professor e do aluno respectivamente;

2. Aluno: possui varias UC's;

3. Professor: possui um horário, sendo que nesse horário estão presentes todas as aulas do docente;

4. Plataforma SIGA: banco de dados e registo de sumários.

**Entidades:**

1. Cartão;

2. Aluno;

3. Professor;

4. Plataforma SIGA.
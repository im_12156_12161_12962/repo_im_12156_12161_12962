# **Entrega 02 - Análise e Planeamento** #

## **Arquitectura funcional** ##
![entrega02.jpg](https://bitbucket.org/repo/9xxgG7/images/2112778170-entrega02.jpg)

## **Modelos de Caso de Uso de cada componente** ##

* ###Registo Presenças ###

![Casos de Uso - Registo de Presenças.jpg](https://bitbucket.org/repo/9xxgG7/images/4058067037-Casos%20de%20Uso%20-%20Registo%20de%20Presen%C3%A7as.jpg)

Código |Nome                                |Breve descrição                                                       |
-------|------------------------------------|----------------------------------------------------------------------|
 UC1.1 |Registar aula                       |Docente abre a aula com recurso ao dispositivo de registo presente em cada sala                                                                                                          |
 UC1.2 |Registar Entrada                    |Aluno regista entrada na aula com recurso ao cartão de identificação  |
 UC1.3 |Registar Dados dos alunos presentes |SIGA guarda informação relativa aos aluno presentes (numero, nome)    |
 UC1.4 |Escrever Sumário                    |Docente atribui sumário à aula respetiva                              |
 UC1.5 |Adicionar Presenças                 |Caso um aluno chegue mais tarde                                       |
 UC1.6 |Validar presenças                   |Docente verifica as presenças dos alunos e valida as mesmas           |



* ### BackOffice ###

![Backofice1.jpg](https://bitbucket.org/repo/9xxgG7/images/3955438175-Backofice1.jpg)

Codigo |Nome                 |Breve descrição                                                                |
-------|---------------------|-------------------------------------------------------------------------------|
 UC2.1 |Gerar Mapa           |SIGA gera mapa de presenças e de sumários relativos a cada aula                |
 UC2.2 |Consultar Mapas      |Funcionário dos serviços consulta mapas gerados para obter informações         |
 UC2.3 |Obter Planificação   |SIGA disponibiliza planificação de aulas aos alunos e docentes                 |
 UC2.4 |Imprimir Mapas       |Funcionário dos serviços imprime mapas gerados                                 |
 UC2.5 |Validar Troca de Aula|Funcionário dos serviços valida possível troca de aula solicitada pelo docente |

## **Requisitos técnicos** ##

* ###Regras de Negócio

Código |Regras De Negocio                                                                            |
-------|---------------------------------------------------------------------------------------------|
 RN01  |O docente possui 10 minutos antes da hora de inicio da aula para registar a mesma            | 
 RN02  |O docente possui 20 minutos depois da hora de inicio da aula para registar a mesma           |
 RN03  |O aluno possui 30 minutos depois do registo da aula para registar a sua presença na mesma    | 
 RN04  |O docente possui 3 dias depois do registo da aula para efectuar o registo do sumário da mesma|

* ### Requisitos Não-Funcionais 

Código |Requisitos Não-Funcionais                                      |
-------|---------------------------------------------------------------|
 RNF01 |Tempo de resposta da maquina não superior a 1 minuto           | 
 RNF02 |O sistema deve estar em concordância com os despachos do reitor|

* ### Pressupostos e Constrangimentos 

Código |Pressupostos e Constrangimentos                                                              |
-------|---------------------------------------------------------------------------------------------|
 PC01  |Existência de um dispositivo que permita o registo de presenças em cada sala                 | 
 PC02  |Tanto o aluno como o docente tem de possuir um cartão de identificação para registar presença|


## **Arquitectura Técnica** ##

![ARQ_TEC.JPG](https://bitbucket.org/repo/9xxgG7/images/1662054105-ARQ_TEC.JPG)

* O sistema escolhido pelo grupo caracteriza-se pela disposição de um dispositivo que permita efectuar registo de aula, que implica para cada docente ter consigo o seu cartão identificador. O mesmo dispositivo tem de permitir também o registo das presenças de cada um dos alunos, para isso, cada aluno terá de possuir tal como o docente o seu cartão de estudante;
* Posteriormente estas informações das presenças bem como o sumário de cada aula estão disponíveis na plataforma SIGA onde podem ser acedidas pelos SA (Serviços Académicos).  

## **Lista prioritária dos casos de uso** ##

1. UC1.1 - Registar Aula
2. UC1.4 - Escrever Sumário
3. UC2.3 - Obter Planificação

## **Casos de uso Prioritários** ##
(1) 

**Código:** UC1.1 

**Nome:** Registar Aula 

**Objectivo:** Abrir aula para permitir marcação de presenças. 

**Actor principal:** Docente

**Outros interessados:** Aluno

**Iniciação:** O Docente carrega no botão verde do dispositivo de registo de presenças e passa o cartão.

**Pré-condições (&pressupostos):** É  necessário  ter um dispositivo de registo de presenças em cada sala e cada docente tem de ter um cartão.

**Cenário Principal:**

1. Docente: Selecciona a opção de abrir aula (carrega no botão verde do dispositivo de registo de presenças). 
2. Docente:Passa o cartão
3. Sistema: Regista dados da aula e do docente

**Pós-condições:** Foi aberta uma nova aula.

(2)

**Código:** UC1.4

**Nome:** Escrever Sumário

**Objetivo:** Relatar por tópicos acontecimentos da aula respetiva

**Ator principal:** Docente

**Outros interessados:** Docente/Aluno

**Iniciação:** O Docente abre a plataforma para escrever o sumário
 
**Pré-condições (&pressupostos):** É  necessário  registar a aula

**Pontos de Inclusão:** inclusão do caso de uso "Validar Presenças"

**Cenário principal:**

1. Docente: Regista a aula 
2. Docente: Faz um apanhado do que se realizou na aula
3. Docente: Regista por tópicos na plataforma tudo o que se passou na aula (prazo de 3 dias a partir do registo da aula)

**Pós-condições:** Foi registado um novo sumário

(3)

**Código:** UC2.3

**Nome:** Obter Planificação

**Objectivo:** Fornecer a planificação das aulas com a respectiva sala e turma

**Actor Principal:** Plataforma SIGA

**Outros interessados:** Docente

**Iniciação:** Plataforma SIGA selecciona o docente em questão e obtem planificação

**Pré-condições (&Pressupostos)**: 

* Pressupõe-se que o docente em questão leccione na instituição respectiva
			           
* O sistema deve responder rapidamente ao planeamento pedido

**Cenário Principal**:

1. Docente: Efectua pedido de obtenção de planificação
2. Plataforma SIGA: Com base nos dados do docente, procura respectivas turmas e diferentes UC's leccionadas, e apresenta a opção de escolher o período a que o docente se refere
3. Docente: Escolhe período que pretende
4. Plataforma SIGA: Apresenta a planificação pedida pelo docente, e encerra o processo

**Fluxos Alternativos:** 

* Em qualquer altura o docente pode terminar o processo:

1. Docente: Selecciona a opção de cancelamento da operação
2. Plataforma SIGA: Cancela o pedido de obtenção de planificação

**Pós-condições:** Foi obtida uma nova planificação